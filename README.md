
# Todo App

A simple Todo app to be evaluated in the React assignment of the P2P project.


## Tech Stack

**Client:** React, ReactDOM, Chakra UI, @testing-library

  
## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/joao.zaniolo/react-assignment
```

Go to the project directory

```bash
  cd react-assignment
```

Install dependencies

```bash
  npm install
```

Start the server

```bash
  npm run start
```

  
## Running Tests

To run tests, run the following command

```bash
  npm run test
```

  
## Authors

- [@jvzaniolo](https://www.github.com/jvzaniolo)

  
