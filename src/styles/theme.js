import { mode } from '@chakra-ui/theme-tools'
import { extendTheme } from '@chakra-ui/react'

export const theme = extendTheme({
  styles: {
    global: props => ({
      body: {
        bg: mode('gray.100', 'gray.900')(props),
        color: mode('gray.900', 'gray.100')(props),
      },
    }),
  },

  shadows: {
    outline: '0 0 0 3px #C6F6D5',
  },

  colors: {
    gray: {
      50: '#FAFAFA',
      100: '#F4F4F5',
      200: '#E4E4E7',
      300: '#D4D4D8',
      400: '#A1A1AA',
      500: '#71717A',
      600: '#52525B',
      700: '#3F3F46',
      800: '#27272A',
      900: '#18181B',
    },
  },

  config: {
    initialColorMode: 'dark',
  },
})
