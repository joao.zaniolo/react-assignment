import { render } from '@testing-library/react'

import { TodoItem } from '../../components/Todo/Item'

const defaultProps = {
  text: 'Walk the dog',
  isDone: false,
  onChange: () => {},
  onDelete: () => {},
}

describe('<TodoItem />', () => {
  it('should render the todo', () => {
    const { getByTestId } = render(<TodoItem {...defaultProps} />)

    expect(getByTestId('todo').querySelector('p')).toHaveTextContent(
      'Walk the dog',
    )
    expect(
      getByTestId('todo').querySelector('input[type="checkbox"]'),
    ).not.toBeChecked()
  })
})
