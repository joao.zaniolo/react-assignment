import { render, waitFor, fireEvent } from '@testing-library/react'

import { TodoList } from '../../components/Todo/List'
import { startMirage } from '../../services/mirage'

let server

beforeEach(() => {
  server = startMirage({ environment: 'test' })
})
afterEach(() => {
  server.shutdown()
})

describe('<TodoList />', () => {
  it('should display a message when the todo list is empty', async () => {
    const { getByTestId } = render(<TodoList />)
    const message = await waitFor(() => getByTestId('no-todos'))

    expect(message).toBeInTheDocument()
  })

  it('should display the existing todos', async () => {
    server.createList('todo', 5)

    const { getAllByTestId } = render(<TodoList />)
    const todos = await waitFor(() => getAllByTestId('todo'))

    expect(todos).toHaveLength(5)
  })

  it('should create a new todo', async () => {
    const { getByTestId, getAllByTestId } = render(<TodoList />)

    const newTodoForm = getByTestId('new-todo-form')

    fireEvent.change(newTodoForm.querySelector('input[type="text"]'), {
      target: {
        value: 'Walk the dog',
      },
    })
    fireEvent.submit(newTodoForm.querySelector('button[type="submit"]'))

    const todos = await waitFor(() => getAllByTestId('todo'))

    expect(todos).toHaveLength(1)
    expect(todos[0].querySelector('p')).toHaveTextContent('Walk the dog')
    expect(todos[0].querySelector('input[type="checkbox"]')).not.toBeChecked()
    expect(newTodoForm.querySelector('input[type="text"]')).toHaveValue('')
  })

  it('should toggle a todo isDone property', async () => {
    server.create('todo', { text: 'Toggle todo test' })

    const { getByTestId } = render(<TodoList />)

    const todo = await waitFor(() => getByTestId('todo'))
    const checkbox = await waitFor(() =>
      getByTestId('toggle-todo').querySelector('input[type="checkbox"]'),
    )

    fireEvent.click(checkbox)

    expect(checkbox).toHaveProperty('checked', true)
    expect(todo.querySelector('del')).toHaveTextContent('Toggle todo test')
    expect(todo.querySelector('del')).toHaveStyle({
      color: 'var(--chakra-colors-gray-400)',
    })
  })

  it('should delete a todo from the list', async () => {
    server.createList('todo', 2)

    const { getAllByTestId } = render(<TodoList />)
    const todos = await waitFor(() => getAllByTestId('todo'))

    expect(todos.length).toBe(2)

    fireEvent.click(todos[0].querySelector('button[aria-label="Delete item"]'))

    const updatedTodos = await waitFor(() => getAllByTestId('todo'))

    expect(updatedTodos.length).toBe(1)
  })
})
