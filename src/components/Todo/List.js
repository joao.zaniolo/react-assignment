import React, { useEffect, useState } from 'react'
import { FiMoon, FiSun } from 'react-icons/fi'
import {
  Button,
  Center,
  Flex,
  FormControl,
  FormErrorMessage,
  Heading,
  HStack,
  Icon,
  IconButton,
  Input,
  Stack,
  Text,
  useColorMode,
  useColorModeValue,
} from '@chakra-ui/react'
import { TodoItem } from './Item'
import { api } from '../../services/axios'

export function TodoList() {
  const { toggleColorMode } = useColorMode()
  const [text, setText] = useState('')
  const [todos, setTodos] = useState([])
  const [isLoading, setIsLoading] = useState(false)
  const [isInputInvalid, setIsInputInvalid] = useState(false)

  useEffect(() => {
    async function fetchTodos() {
      try {
        setIsLoading(true)
        const response = await api.get('/todos')

        setTodos(response.data.todos)
      } catch (err) {
        alert(err)
      } finally {
        setIsLoading(false)
      }
    }

    fetchTodos()

    return () => {
      setTodos([])
    }
  }, [])

  async function handleNewTodo(event) {
    try {
      event.preventDefault()
      if (text.trim() === '') {
        setIsInputInvalid(true)
        return
      }
      setIsLoading(true)

      const response = await api.post('todos', {
        todo: {
          text,
        },
      })

      setTodos([response.data.todo, ...todos])
      setText('')
    } catch (err) {
      alert(err.message)
    } finally {
      setIsLoading(false)
    }
  }

  async function handleToggleTodo({ id, isDone }) {
    try {
      setIsLoading(true)

      setTodos(
        todos.map(todo =>
          todo.id === id ? { ...todo, isDone: !isDone } : todo,
        ),
      )
      await api.patch(`/todos/${id}`, {
        todo: {
          isDone: !isDone,
        },
      })
    } catch (err) {
      alert(err.message)
    } finally {
      setIsLoading(false)
    }
  }

  async function handleDeleteTodo(id) {
    try {
      setIsLoading(true)
      setTodos(todos.filter(todo => todo.id !== id))
      await api.delete(`/todos/${id}`)
    } catch (err) {
      alert(err)
    } finally {
      setIsLoading(false)
    }
  }

  function handleInputChange(event) {
    setIsInputInvalid(false)
    setText(event.target.value)
  }

  return (
    <Stack
      py="4"
      px="6"
      my="8"
      maxW={500}
      minH={300}
      mx={['8', '8', 'auto']}
      rounded="lg"
      shadow="lg"
      spacing="4"
      bg={useColorModeValue('gray.50', 'gray.800')}
    >
      <Flex align="center" justify="space-between">
        <Heading>Todo List</Heading>
        <IconButton
          rounded="full"
          aria-label="Switch theme"
          onClick={toggleColorMode}
          icon={<Icon as={useColorModeValue(FiSun, FiMoon)} />}
        />
      </Flex>

      <HStack
        as="form"
        onSubmit={handleNewTodo}
        align="flex-start"
        data-testid="new-todo-form"
      >
        <FormControl isInvalid={isInputInvalid}>
          <Input
            name="todo-text"
            type="text"
            value={text}
            disabled={isLoading}
            focusBorderColor="green.400"
            onChange={handleInputChange}
            placeholder="Add cheetos to shopping list"
            variant={useColorModeValue('outline', 'filled')}
          />
          <FormErrorMessage>Insert a content to the todo item</FormErrorMessage>
        </FormControl>
        <Button px="6" type="submit" colorScheme="green" isLoading={isLoading}>
          New todo
        </Button>
      </HStack>

      {todos.length > 0 ? (
        <Stack>
          {todos.map(todo => (
            <TodoItem
              key={todo.id}
              text={todo.text}
              isDone={todo.isDone}
              onChange={() => handleToggleTodo(todo)}
              onDelete={() => handleDeleteTodo(todo.id)}
            />
          ))}
        </Stack>
      ) : (
        <Center flex="1">
          <Text as="em" color="gray.400" size="md" data-testid="no-todos">
            Create a new todo item...
          </Text>
        </Center>
      )}
    </Stack>
  )
}
