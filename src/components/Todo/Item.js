import { FiTrash } from 'react-icons/fi'
import {
  Checkbox,
  Flex,
  Icon,
  IconButton,
  Text,
  Tooltip,
  useColorModeValue,
} from '@chakra-ui/react'

export function TodoItem({ text, isDone, onChange, onDelete }) {
  return (
    <Flex
      px="4"
      py="2"
      shadow={useColorModeValue('md', 'lg')}
      justify="space-between"
      rounded="md"
      data-testid="todo"
    >
      <Checkbox
        size="md"
        isChecked={isDone}
        onChange={onChange}
        colorScheme="green"
        data-testid="toggle-todo"
      >
        <Text as={isDone ? 'del' : 'p'} ml="1" color={isDone ? 'gray.400' : ''}>
          {text}
        </Text>
      </Checkbox>
      <Tooltip label="Delete item" aria-label="Delete item" hasArrow>
        <IconButton
          color="red.400"
          rounded="full"
          variant="ghost"
          aria-label="Delete item"
          onClick={onDelete}
          icon={<Icon as={FiTrash} />}
        />
      </Tooltip>
    </Flex>
  )
}
