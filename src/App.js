import { ChakraProvider } from '@chakra-ui/react'
import { startMirage } from './services/mirage'

import { theme } from './styles/theme'
import { TodoList } from './components/Todo/List'

startMirage()

export function App() {
  return (
    <ChakraProvider theme={theme}>
      <TodoList />
    </ChakraProvider>
  )
}
