import { createServer, Model, Factory } from 'miragejs'

export function startMirage({ environment = 'development' } = {}) {
  return createServer({
    environment,

    models: {
      todo: Model,
    },

    factories: {
      todo: Factory.extend({
        text(i) {
          return `Todo ${i + 1}`
        },
        isDone: false,
      }),
    },

    seeds(server) {
      server.create('todo', { text: 'Go shopping' })
      server.create('todo', { text: 'Walk the dog' })
      server.create('todo', { text: 'Do laundry', isDone: true })
    },

    routes() {
      this.namespace = 'api'
      this.urlPrefix = 'http://localhost:3000'

      this.get('/todos', schema => {
        return schema.todos.all()
      })
      this.post('/todos', (schema, request) => {
        const { text } = JSON.parse(request.requestBody).todo

        return schema.todos.create({ text, isDone: false })
      })
      this.patch('/todos/:id', (schema, request) => {
        const { isDone } = JSON.parse(request.requestBody).todo

        return schema.todos.find(request.params.id).update('isDone', isDone)
      })
      this.del('/todos/:id', (schema, request) => {
        return schema.todos.find(request.params.id).destroy()
      })
    },
  })
}
